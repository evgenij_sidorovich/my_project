import sqlite3


def countries():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
                CREATE TABLE IF NOT EXISTS Countries (
                name TEXT PRIMARY KEY 
                );
            """

    curs.execute(query)
    res = curs.fetchall()
    print(res)


def insert_country():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
            INSERT INTO Countries(name)
            VALUES
            ('Belarus'),
            ('Poland'),
            ('Ukraine'),
            ('Moldova'),
            ('Russia'),
            ('USA'),
            ('Turkey'),
            ('Germany')  
                     
            """

    curs.execute(query)

    query = """
            select * from Countries;
            """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('COMMIT')


def update_countries():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
            UPDATE Countries
            SET name = 'Latvia' where name = 'Moldova'
    """
    curs.execute(query)

    query = """
            select * from Countries;
            """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('Commit')


def delete_countries():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
            DELETE from Countries
            where name = 'Turkey'
    """
    curs.execute(query)

    query = """
            select * from Countries;
            """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('COMMIT')


countries()
insert_country()
update_countries()
delete_countries()
