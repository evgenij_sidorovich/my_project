import socket
import logging
from time import sleep


def main():
    sock = socket.socket()
    sock.connect(('localhost', 9090))

    try:
        requests = ['get_C', 'get_F', 'get_date', 'get_time', 'get_datetime', 'GET_HIST']
        for req in requests:
            sock.send(req.encode('utf-8'))
            sleep(1)

            data = sock.recv(1024)
            print(data)

            logging.basicConfig(filename='logfile.log',
                                format=f'{req} - %(asctime)s - {data}',
                                level=logging.DEBUG,
                                )
            logging.debug("Log in file")
    finally:
        sock.close()


if __name__ == '__main__':
    main()



