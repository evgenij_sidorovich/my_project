from random import choice


class Car:
    def __init__(self, brand, distance, tank=60):
        self.brand = brand
        self.tank = tank
        self.distance = distance
        self.start_distance = 0
        self.current_speed = 0
        self.start_speed = 30
        self._max_speed = 180
        self._max_tank = 60
        self.hour_way = 0
        self.rest_turn = 900
        self.road_sign = [40, 60, 90, 120]

    def way_sign(self):
        return choice(self.road_sign)

    def fuel_finish_error(self):
        if self.tank <= 0:
            try:
                raise Exception
            except Exception:
                print('FuelFinishError')
            finally:
                self.tank = 60
                self.current_speed = 30

    def limit_speed(self):
        if self.current_speed > self._max_speed:
            try:
                raise Exception("SpeedLimitError")
            except Exception:
                print('SpeedLimitError')
            finally:
                self.current_speed = 120

    def limit_speed_40(self):
        self.tank -= (10 * self.current_speed) / 100
        self.start_distance += self.current_speed

    def limit_speed_60(self):
        self.tank -= (8 * self.current_speed) / 100
        self.start_distance += self.current_speed

    def limit_speed_90(self):
        self.tank -= (6 * self.current_speed) / 100
        self.start_distance += self.current_speed

    def limit_speed_120(self):
        self.tank -= (5.5 * self.current_speed) / 100
        self.start_distance += self.current_speed

    def limit_speed_140(self):
        self.tank -= (8 * self.current_speed) / 100
        self.start_distance += self.current_speed

    def limit_speed_180(self):
        self.tank -= (11 * self.current_speed) / 100
        self.start_distance += self.current_speed

    def sign_40(self):
        self.current_speed = 40
        self.tank -= (8 * self.current_speed) / 100

    def sign_60(self):
        self.current_speed = 60
        self.tank -= (6 * self.current_speed) / 100

    def sign_90(self):
        self.current_speed = 90
        self.tank -= (5.5 * self.current_speed) / 100

    def sign_120(self):
        self.current_speed = 120
        self.tank -= (8 * self.current_speed) / 100

    def action_car(self):
        self.hour_way = 0
        self.start_speed = self.current_speed = 30
        while True:
            if 0 <= self.current_speed < 40:
                self.limit_speed_40()
                self.fuel_finish_error()
                if self.start_distance >= self.distance:
                    print('StopDrive')
                    break
            elif 40 <= self.current_speed < 60:
                self.limit_speed_60()
                self.fuel_finish_error()
                if self.start_distance >= self.distance:
                    print('StopDrive')
                    break
            elif 60 <= self.current_speed < 90:
                self.limit_speed_90()
                self.fuel_finish_error()
                if self.start_distance >= self.distance:
                    print('StopDrive')
                    break
            elif 90 <= self.current_speed < 120:
                self.limit_speed_120()
                self.fuel_finish_error()
                if self.start_distance >= self.distance:
                    print('StopDrive')
                    break
            elif 120 <= self.current_speed < 140:
                self.limit_speed_140()
                self.fuel_finish_error()
                if self.start_distance >= self.distance:
                    print('StopDrive')
                    break
            elif 140 <= self.current_speed < 180:
                self.limit_speed_180()
                self.fuel_finish_error()
                if self.start_distance >= self.distance:
                    print('StopDrive')
                    break

            sign = self.way_sign()
            print(f'Front sign {sign}')

            if sign == 40:
                if self.current_speed > sign:
                    self.sign_40()
            elif sign == 60:
                if self.current_speed > 60:
                    self.sign_60()
            elif sign == 90:
                if self.current_speed > 90:
                    self.sign_90()
            elif sign == 120:
                if self.current_speed > 120:
                    self.sign_120()

            print(f'Your speed {self.current_speed} km/h')
            print(f'You drove {self.start_distance} km')
            print(f'You are driving {self.hour_way} hour')
            print(f'You have {self.tank} litres fuel')

            self.hour_way += 1
            self.current_speed += 5
            self.limit_speed()


car_1 = Car('BMW', 2000)
car_1.action_car()
