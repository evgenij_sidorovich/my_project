# Ex.2
# Задача
# Найти сумму всех чисел, меньших 1000, кратных 3 и 7
# Реализовать через filter/map/reduce

from functools import reduce

numbers: list = list(range(1, 1000))


def selects_numbers(*args):
    lst_numbers = list(map(int, filter(lambda x: (x % 3 == 0 and x % 7 == 0), *args)))
    sum_numbers = reduce(lambda x, y: x + y, lst_numbers)
    return sum_numbers


def test_select_numbers():
    assert 23688 == selects_numbers(numbers)


print(selects_numbers(numbers))
