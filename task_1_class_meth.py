# Ex.1
# реализовать общий для всех классов
# counter и метод, который его изменяет на 1
# изменения должны быть видны всем дочерним классам

class School:
    counter = 10

    @classmethod
    def incr_counter(cls):
        cls.counter += 1


class Group(School):
    @classmethod
    def incr_counter(cls):
        return super().incr_counter()


cls_1 = School()
cls_2 = School()
cls_1.incr_counter()

group_1 = Group()


def test_meth():
    assert cls_1.counter == cls_2.counter == group_1.counter


test_meth()
