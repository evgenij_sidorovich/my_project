# Таблицу Jobs
# - title
# - min_salary
# - max_salary

import sqlite3


def jobs():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
                CREATE TABLE IF NOT EXISTS Jobs (
                title TEXT PRIMARY KEY,
                min_salary INTEGER,
                max_salary INTEGER
                );
            """

    curs.execute(query)
    res = curs.fetchall()
    print(res)


def insert_jobs():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()
    query = """
            INSERT INTO Jobs (title, min_salary, max_salary)
            VALUES
            ('manager', 1200, 1800),
            ('master', 1350, 1900),
            ('HR', 1400, 2400),
            ('pr-manager', 1600, 2900)
            
            """

    curs.execute(query)

    query = """
            select * from Jobs;
            """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('Commit')


def update_job():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
                UPDATE Jobs
                SET  min_salary = 1340 where title = 'HR'
        """
    curs.execute(query)

    query = """
                select * from Jobs;
                """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('Commit')


def delete_job():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
            DELETE from Jobs
            where min_salary < 1300
    """
    curs.execute(query)

    query = """
                select * from Jobs;
                """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('Commit')


# jobs()
# insert_jobs()
# update_job()
# delete_job()


# Реализовать функцию, возвращающую среднее значение зарплаты

def avg_salary():
    """Function return avg salary"""
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
                    select AVG(max_salary - min_salary) from Jobs as avg;
                        """

    curs.execute(query)
    res = curs.fetchall()
    print(res)


avg_salary()


