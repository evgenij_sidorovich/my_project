# Ex.4
# Реализовать дата-класс HostSettings.
# Должен иметь свойства ip_addr, port, hostname, username, password, auth_type

from dataclasses import dataclass


@dataclass
class HostSettings:
    ip_addr: str
    post: str
    host_name: str
    username: str
    password: str
    auth_type: str


hostSettings_1 = HostSettings(ip_addr='127.0.0.1', post='Server 1', host_name='host 2',
                              username='Alex', password='123abc', auth_type='guest 2')

hostSettings_2 = HostSettings(ip_addr='10.10.21.85', post='Server 2', host_name='host 2',
                              username='Bob', password='qwerty123', auth_type='guest 2')
print(hostSettings_1)
print(hostSettings_2)


assert hostSettings_1.__dict__ == {'ip_addr': '127.0.0.1', 'post': 'Server 1', 'host_name': 'host 2',
                                   'username': 'Alex', 'password': '123abc', 'auth_type': 'guest 2'}

assert hostSettings_1 != hostSettings_2
