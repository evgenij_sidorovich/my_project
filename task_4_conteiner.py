# Ex.4
# Класс-контейнер хранит экзепмляры класса Shape.
# Реализациями Shape могут быть 'Rectange', 'Triangle', 'Circle'
# Shape должен предоставлять интерфейс square
# Предусмотреть проверки
# Контейнер должен иметь свойство square - возвращающее общую площадь всех фигур

import abc
import math


class ABCShape(abc.ABC):

    @abc.abstractmethod
    def square(self):
        pass


class Rectangle(ABCShape):
    def __init__(self, side_1, side_2):
        self.side_1 = side_1
        self.side_2 = side_2

    def square(self):
        return self.side_1 * self.side_2


class Triangle(ABCShape):
    def __init__(self, base, hight):
        self.base = base
        self.hight = hight

    def square(self):
        return (self.base * self.hight) / 2


class Circle(ABCShape):
    def __init__(self, radius):
        self.radius = radius

    def square(self):
        return math.pi * self.radius ** 2


class ShapeContainer:
    def __init__(self):
        self._sum_square = []

    def add_sum_square(self, shape):
        self._sum_square.append(shape)
        return sum(self._sum_square)

    @property
    def sum_square_shape(self):
        return sum(self._sum_square)

    @property
    def sum_square(self):
        return self._sum_square


sc = ShapeContainer()
r = Rectangle(3, 4)
t_1 = Triangle(2, 8)
t_2 = Triangle(2, 5)
c = Circle(3)
sc.add_sum_square(r.square())
sc.add_sum_square(t_1.square())
sc.add_sum_square(t_2.square())
sc.add_sum_square(c.square())
print(sc.sum_square)
print(sc.sum_square_shape)
