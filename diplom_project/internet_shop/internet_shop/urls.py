from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    path('modakids/', include('shop.urls')),
    path('modakids/cart/', include('cart.urls')),
    path('modakids/orders/', include('orders.urls')),
    path('modakids/account/', include('account.urls')),
    path('modakids/stock/', include('stock.urls'))
]


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

