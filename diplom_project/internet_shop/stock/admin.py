from django.contrib import admin
from .models import Stock


class StockAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'period']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Stock, StockAdmin)
