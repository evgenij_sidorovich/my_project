from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404
from .models import Stock


def stock_catalog(request):
    stocks = Stock.objects.all()

    paginator = Paginator(stocks, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request,
                  'stock.html',
                  {'stocks': stocks,
                   'page_obj': page_obj})


def stock_detail(request, id, slug):
    stock = get_object_or_404(Stock,
                              id=id,
                              slug=slug)
    return render(request,
                  'stock_detail.html',
                  {'stock': stock})
