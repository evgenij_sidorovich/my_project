from django.urls import re_path
from . import views


urlpatterns = [
    re_path(r'^$', views.stock_catalog, name='stock_catalog'),
    re_path('(?P<id>\d+)/(?P<slug>[-\w]+)/', views.stock_detail, name='stock_detail')
]
