# Generated by Django 4.0.4 on 2022-06-17 16:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0011_comments'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Comments',
            new_name='Comment',
        ),
    ]
