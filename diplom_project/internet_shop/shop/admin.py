from django.contrib import admin
from .models import Category, Product, Contact


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Category, CategoryAdmin)


class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'price', 'available']
    list_filter = ['available']
    list_editable = ['price', 'available']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Product, ProductAdmin)


class ContactAdmin(admin.ModelAdmin):
    list_display = ['name', 'telephone', 'subject', 'message']


admin.site.register(Contact, ContactAdmin)

#
# class CommentAdmin(admin.ModelAdmin):
#     list_display = ('user', 'body', 'created', 'active')
#     list_filter = ('active', 'created')
#     search_fields = ('name', 'body')
#
#
# admin.site.register(Comment, CommentAdmin)
