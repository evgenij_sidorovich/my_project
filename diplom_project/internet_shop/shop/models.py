from django.db import models
from django.urls import reverse


class Category(models.Model):
    name = models.CharField(max_length=50, db_index=True)
    slug = models.SlugField(max_length=50, db_index=True, unique=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('product_list_by_category',
                        args=[self.slug])


class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products', on_delete=models.CASCADE)
    name = models.CharField(max_length=200, db_index=True)
    slug = models.SlugField(max_length=200, db_index=True)
    image = models.ImageField(upload_to='products/%Y/%m/%d', blank=True)
    description = models.TextField(blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    available = models.BooleanField(default=True)

    class Meta:
        ordering = ('name',)
        index_together = (('id', 'slug'),)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('product_detail',
                       args=[self.id, self.slug])


class Contact(models.Model):
    name = models.CharField(max_length=50)
    telephone = models.CharField(max_length=50)
    subject = models.CharField(max_length=50)
    message = models.CharField(max_length=100)

    def __str__(self):
        return self.subject

#
# class Comment(models.Model):
#     user = models.CharField(max_length=80)
#     body = models.TextField()
#     created = models.DateTimeField(auto_now_add=True)
#     active = models.BooleanField(default=True)
#
#     class Meta:
#         ordering = ('created',)
#
#     def __str__(self):
#         return self.body

