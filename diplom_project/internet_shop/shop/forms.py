from django import forms
from django.forms import Textarea, TextInput
from .models import Contact


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = ['name', 'telephone', 'subject', 'message']
        labels = {
            'name': 'Имя',
            'telephone': 'Телефон',
            'subject': 'Тема сообщения',
            'message': 'Сообщение'}
        widgets = {
            'message': Textarea(attrs={
                'placeholder': 'Напишите ваше сообщение'}),
            'name': TextInput(attrs={
                    'placeholder': 'Введите имя...'}),
            'telephone': TextInput(attrs={
                'placeholder': 'Введите телефон...'}),
            'subject': TextInput(attrs={
                'placeholder': 'Введите тему...'})
        }

#
# class CommentForm(forms.ModelForm):
#     class Meta:
#         model = Comment
#         fields = ('user', 'created', 'body')
