from django.urls import re_path
from . import views
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    re_path('^$', views.main, name='main'),
    re_path('about/', views.about, name='about'),
    path('information/', include([
        re_path('delivery/', views.delivery, name='delivery'),
        re_path('payment/', views.payment, name='payment'),
        re_path('return/', views.contact_view, name='return'),
        re_path('contacts/', views.contacts, name='contacts'),
        re_path('thanks/', views.thanks, name='thanks')
    ])),
    path('category/', include([
        re_path('^$', views.catalog, name='catalog'),
        re_path('(?P<id>\d+)/(?P<slug>[-\w]+)/', views.product_detail, name='product_detail'),
        re_path('(?P<category_slug>[-\w]+)/', views.product_list, name='product_list_by_category'),
    ])),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


