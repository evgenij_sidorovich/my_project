from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.views.generic import ListView

from .forms import ContactForm
from .models import Category, Product
from cart.forms import CartAddProductForm


def main(request):
    return render(request, "main.html")


def about(request):
    return render(request, "about.html")


def delivery(request):
    return render(request, "delivery.html")


def payment(request):
    return render(request, "payment.html")


# def return_things(request):
#     return render(request, "return.html")


def contacts(request):
    return render(request, "contacts.html")


def catalog(request):
    category = None
    categories = Category.objects.all()
    products = Product.objects.all()

    paginator = Paginator(products, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    cart_product_form = CartAddProductForm()

    return render(request,
                  'catalog.html',
                  {'category': category,
                   'categories': categories,
                   'products': products,
                   'page_obj': page_obj,
                   'cart_product_form': cart_product_form})


def product_list(request, category_slug=None):
    categories = Category.objects.all()
    category = get_object_or_404(Category, slug=category_slug)

    products = Product.objects.filter(category=category)
    paginator = Paginator(products, 6)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    cart_product_form = CartAddProductForm()
    return render(request,
                  'category/all_products.html',
                  {'category': category,
                   'categories': categories,
                   'products': products,
                   'page_obj': page_obj,
                   'cart_product_form': cart_product_form})


def product_detail(request, id, slug):
    product = get_object_or_404(Product,
                                id=id,
                                slug=slug,
                                available=True)
    cart_product_form = CartAddProductForm()
    return render(request,
                  'category/details.html',
                  {'product': product,
                   'cart_product_form': cart_product_form})


def contact_view(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)

        # Если форма заполнена корректно, сохраняем все введённые пользователем значения
        if form.is_valid():
            form_data = form.cleaned_data
            form.save()
            # Переходим на другую страницу, если сообщение отправлено
            return HttpResponseRedirect('/modakids/information/thanks/')
    else:
        # Заполняем форму
        form = ContactForm()
    # Отправляем форму на страницу
    return render(request, 'return.html', {'form': form})


def thanks(request):
    return render(request, 'thanks.html')


# def post_detail(request):
#
#     if request.method == 'POST':
#         comment_form = Comment(data=request.POST)
#         if comment_form.is_valid():
#             new_comment = comment_form.save(commit=False)
#             new_comment.save()
#     else:
#         comment_form = Comment()
#     return render(request,
#                   'detail.html',
#                  {'comment_form': comment_form})