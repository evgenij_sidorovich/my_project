from django import forms
from .models import Order
from django.utils.translation import gettext_lazy as _


class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['first_name', 'last_name', 'email', 'address', 'postal_code', 'city']
        labels = {
            'first_name': _('Имя'),
            'last_name': _('Фамилия'),
            'email': _('Эл. почта'),
            'address': _('Адрес'),
            'postal_code': _('Индекс'),
            'city': _('Город'),
        }
