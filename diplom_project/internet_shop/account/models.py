from django.db import models


class LoginUser(models.Model):
    username = models.CharField(max_length=20, db_index=True)
    password = models.CharField(max_length=50, db_index=True)

    def __str__(self):
        return self.username
