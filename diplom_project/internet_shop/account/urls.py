import django
from django.urls import re_path
from . import views


app_name = 'account'

urlpatterns = [
    re_path(r'login/', views.user_login, name='login'),
    re_path(r'logout/', views.logout_user, name='logout'),
    re_path(r'register/', views.register, name='register'),
    re_path(r'invalid_login/', views.invalid_login, name='invalid_login'),

]
