# Ex1
# Класс NamesIterable принимает коллекцию Фамилия Имя Отчество.
# Реализовать итератор, который будет возвращать элементы, ориентируясь на Имя
# Разделители могут быть ' ', '_', '-'
# names = ['Ivanov Ivan Ivanovich', 'Petrova Olga Sergeevna', 'Petrichenko_Olga_Vladimirovna']
#
# iterable = NamesIterable(names, 'Olga')
# iterator = iter(iterable)
# assert list(iterator) == ['Petrova Olga Sergeevna', 'Petrichenko_Olga_Vladimirovna']

from collections.abc import Iterator, Iterable

names = ['Ivanov Ivan Ivanovich', 'Ivanova-Olga-Ivanovna',
         'Petrova Olga Sergeevna', 'Petrichenko_Olga_Vladimirovna']


class ListIterator(Iterator):
    def __init__(self, collection, cursor):
        self._collection = collection
        self._cursor = cursor

    def __next__(self):
        if (self._cursor + 1) >= len(self._collection):
            raise StopIteration
        self._cursor += 1
        return self._collection[self._cursor]


class NamesIterable(Iterable):
    _IteratorCls = ListIterator

    def __init__(self, collection):
        self._collection = collection

    def __iter__(self):
        return self._IteratorCls(self._collection, -1)


aggregate = NamesIterable(names)
iterator = iter(aggregate)
res = list(iterator)

for name in res:
    if 'Olga' in name:
        print(name)
