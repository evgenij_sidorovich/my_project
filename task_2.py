# -- Ex.2
# -- Найти заказчиков, у которых нет ни одного заказа. Вывести имя заказчика и customer_id.

import psycopg2
from session import open_db_session
from models import Customers, Orders


def get_customers_without_order():
    with open_db_session() as session:
        query = (session.query(Customers.company_name, Customers.customer_id).
                 outerjoin(Orders, Customers.customer_id == Orders.customer_id).
                 filter(Orders.order_id == None).all())

        for i in query:
            print(i)


# get_customers_without_order()


def get_customer_with_employer():
    conn = psycopg2.connect(dbname='tech', user='tech',
                            password='tech', host='localhost', port='8432')
    cursor = conn.cursor()

    query = """
            select c.company_name, c.customer_id from customers as c
            left join orders as o using (customer_id)
            where o.order_id is Null 
    """

    cursor.execute(query)
    res = cursor.fetchall()
    print(res)

    cursor.execute('COMMIT')


get_customer_with_employer()