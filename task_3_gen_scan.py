# Ex.3
# Написать функцию-генератор, которая сканирует папки/файлы
# и возвращает полный путь к файлам, размер которых больше 1 МБ
# '/ful_path_to_file', 'file_size'
# Результат записывается в файл out.txt

import os

lst_files = os.listdir('.')


def gen_scan(lst_files):
    for file in lst_files:
        if os.path.getsize(file) // 1024 > 1:
            yield os.path.abspath(file), os.path.getsize(file) // 1024


gen = gen_scan(lst_files)


file_book = open('out.txt', 'w')
for info in gen:
    file_book.write(str(info) + '\n')
file_book.close()
