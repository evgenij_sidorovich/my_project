# Ex.6
# Реализовать класс-миксин, добавляющий классу Car атрибут
# spoiler.
# Spoiler должен влиять на Car.speed , увеличивая ее на значение N.

class Spoiler:
    def __init__(self, spoiler):
        self.spoiler = spoiler

    def on(self):
        print('Car with spoiler')

    def off(self):
        print('Car without spoiler')

    def speed(self, increase_speed):
        self.speed += increase_speed
