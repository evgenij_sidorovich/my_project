from contextlib import contextmanager
from typing import ContextManager

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

import settings as app_settings

engine = create_engine(app_settings.DB_CONNECTION)
DBSession = sessionmaker(bind=engine)


@contextmanager
def open_db_session(with_commit=False) -> ContextManager[Session]:
    session = DBSession()
    try:
        yield session
        if with_commit:
            session.commit()
    except:
        session.rollback()
        raise

    session.close()
