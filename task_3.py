# Ex.3
# Найти активные (поле discontinued) продукты из категории Condiments и Meat/Poultry,
# которых в продаже менее 100 единиц
# Вывести наименование продуктов, кол-во единиц в продаже,
# имя контакта поставщика и его телефонный номер.

import psycopg2
from sqlalchemy import or_, and_
from session import open_db_session
from models import Categories, Products, Suppliers


def activ_products_from_category():
    with open_db_session() as session:
        query = (session.query(Products.product_name, Products.units_in_stock, Suppliers.company_name, Suppliers.phone).
                 join(Suppliers, Products.product_id == Suppliers.supplier_id).
                 join(Categories, Products.category_id == Categories.category_id).
                 filter(or_(Categories.category_name == 'Condiments', Categories.category_name == 'Meat/Poultry')).
                 filter(and_(Products.discontinued > 0, Products.units_in_stock < 100)).all())

        for i in query:
            print(i)


activ_products_from_category()


def activ_products():
    conn = psycopg2.connect(dbname='tech', user='tech',
                            password='tech', host='localhost', port='8432')
    cursor = conn.cursor()

    query = """
            select p.product_name, p.units_in_stock, s.company_name, s.phone from products as p
            inner join suppliers s using (supplier_id)
            inner join categories ct using (category_id)
            where p.discontinued > 0 and ct.category_name = 'Condiments' 
            or ct.category_name = 'Meat/Poultry,' and p.units_in_stock < 100
            order by p.product_name 
    """

    cursor.execute(query)
    res = cursor.fetchall()
    print(res)


activ_products()
