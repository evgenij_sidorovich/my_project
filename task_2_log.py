# Ex.2
# реализовать класс, который имеет атрибут журнала логирования,
# куда попадают строки вида
# <дата-время вызова>-<класс>-<вызываемый метод>
# * реализовать декоратор, которым можно будет оборачивать тот метод,который мы захотим залогировать


import logging
from time import asctime


class logging_book:
    def __init__(self, log_book):
        self.log_book = log_book

    def decor_log(method):
        def wrapper(self):
            return logging.basicConfig(level=logging.DEBUG, filename=self.log_book,
                                       format=f'{asctime} - {logging_book.__name__} - {method.__name__}')

        return wrapper
