from django.urls import re_path, path
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    re_path('', views.index, name='main page'),
]
