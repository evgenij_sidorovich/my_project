# Таблицу Location
# - street_address
# - postal_code
# - city
# - country_id

import sqlite3


def location():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
                CREATE TABLE IF NOT EXISTS Location (
                country_id TEXT PRIMARY KEY, 
                street_address TEXT,
                postal_code INTEGER,
                city TEXT
                );
            """

    curs.execute(query)
    res = curs.fetchall()
    print(res)


def insert_location():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()
    query = """
            INSERT INTO Location (country_id, street_address, postal_code, city)
            VALUES
            ('Belarus', 'Mogilevskaya', '220023', 'Minsk'),
            ('Poland', 'Warshawska', '189234', 'Gdansk'),
            ('Ukraine', 'Volynska', '245532', 'Kiev'),
            ('Moldova', 'Skaryny', '220019', 'Kishenev'),
            ('Russia', 'Moskovskaya', '123123', 'Sochy')           

            """

    curs.execute(query)

    query = """
            select * from Location;
            """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('COMMIT')


def update_department():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
            UPDATE Location
            SET  postal_code = 233203 where street_address = 'Volynska'
    """
    curs.execute(query)

    query = """
            select * from Location;
            """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('Commit')


def delete_location():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
            DELETE from Location
            where country_id = 'Russia'
    """
    curs.execute(query)

    query = """
            select * from Location;
            """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('COMMIT')


location()
insert_location()
update_department()
delete_location()


# Реализовать функцию, возвращающую список городов в алфавитном порядке. Учесть дубликаты

def list_cities():
    """Function return list cities in alphabet order"""
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
                 select distinct(city) from Location
                 order by city
            """

    curs.execute(query)
    res = curs.fetchall()
    print(res)


list_cities()
