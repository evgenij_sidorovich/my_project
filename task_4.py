# -- Ex.4
# -- Подсчитать количество товаров в каждой категории, вывести по убыванию.

import psycopg2
from sqlalchemy import desc
from sqlalchemy.sql.expression import func
from session import open_db_session
from models import Categories, Products


def count_products():
    with open_db_session() as session:
        query = (session.query(Categories.category_name, func.sum(Products.units_in_stock)).
                 join(Products, Categories.category_id == Products.category_id).
                 group_by(Categories.category_name).
                 order_by(desc(func.sum(Products.units_in_stock))).all())

        for i in query:
            print(i)


# count_products()


def count_product_in_category():
    conn = psycopg2.connect(dbname='tech', user='tech',
                            password='tech', host='localhost', port='8432')
    cursor = conn.cursor()

    query = """
            select category_name, sum(units_in_stock) from products as p
            inner join categories as c using (category_id)
            group by category_name
            order by sum(units_in_stock) DESC
    """

    cursor.execute(query)
    res = cursor.fetchall()

    for i in res:
        print(i)


count_product_in_category()
