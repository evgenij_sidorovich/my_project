from datetime import datetime
import socket


def main():
    sock = socket.socket()
    sock.bind(('localhost', 9090))
    sock.listen(3)
    conn, addr = sock.accept()

    try:
        while True:
            client_data = conn.recv(1024)
            print(client_data)

            if not client_data:
                break
            else:
                if client_data == b'get_C':
                    conn.send('Temp 100 C'.encode('utf_8'))
                elif client_data == b'get_F':
                    conn.send('Temp 100 F'.encode('utf_8'))
                elif client_data == b'get_date':
                    conn.send(f'{datetime.now().date()}'.encode('utf_8'))
                elif client_data == b'get_time':
                    conn.send(f'{datetime.now().time()}'.encode('utf_8'))
                elif client_data == b'get_datetime':
                    conn.send(f'{datetime.now()}'.encode('utf_8'))
                elif client_data == b'GET_HIST':
                    with open('logfile.log') as file:
                        conn.send(f'{file.readlines()}'.encode('utf-8'))

    except KeyboardInterrupt:
        print('server finished')
        conn.close()


if __name__ == '__main__':
    main()
