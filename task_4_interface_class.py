# Ex.4
# Реализовать интерфейс класса Car

import abc


class ABCCar(abc.ABC):
    @abc.abstractmethod
    def forward(self):
        print(f'Car moving forward')

    @abc.abstractmethod
    def stop(self):
        print(f'Car stopped')


class Car(ABCCar):
    def __init__(self, brand):
        self.brand = brand
        self.speed = 0
        self.max_speed = 240

    def forward(self):
        print(f'Car {self.brand} start of moving')

    def stop(self):
        print(f'Car {self.brand} stopped')


car_1 = Car('KIA')
car_1.forward()
