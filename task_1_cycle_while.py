# Ex.1
# Loops
# Написать цикл, который опрашивает пользователя и
# выводит рандомное число.
# Цикл должен прерываться по символу Q(q)

from random import randint

while True:
    enter_string = str(input('Enter your string or "q" for exit: '))
    if enter_string.upper() == 'Q':
        print('End')
        break
    else:
        print(randint(1, 100))

