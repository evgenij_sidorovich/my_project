# Ex.1 - реализовать калькулятор с обработкой ошибок,
# сохранением результата предыдущей операции,
# сохранением истории вычислений и возможностью вернуться к n-1 шагу.

class Calculator:
    LAST_VALUE = None

    def __init__(self):
        self._memory = []
        self.sign = ('+', '-', '*', '/')

    def sum_numbers(self, number_1, number_2):
        self._memory.append(number_1 + number_2)
        self.LAST_VALUE = self._memory[-1]
        return number_1 + number_2

    def difference_numbers(self, number_1, number_2):
        self._memory.append(number_1 - number_2)
        self.LAST_VALUE = self._memory[-1]
        return number_1 - number_2

    def mul_numbers(self, number_1, number_2):
        self._memory.append(number_1 * number_2)
        self.LAST_VALUE = self._memory[-1]
        return number_1 * number_2

    def div_numbers(self, number_1, number_2):
        try:
            self._memory.append(number_1 / number_2)
            self.LAST_VALUE = self._memory[-1]
            return number_1 / number_2
        except ZeroDivisionError as e:
            print(f"You have problem {e}")

    def clean_memory(self):
        return self._memory == []

    def jump_to_value(self, n):
        try:
            return self._memory[n - 1]
        except IndexError:
            print('Given value does not exist')

    def memory(self):
        return self._memory


s1 = Calculator()

s1.sum_numbers(2, 3)
s1.difference_numbers(10, 6)
s1.mul_numbers(5, 9)
s1.div_numbers(8, 0)

print(s1.memory)
print(s1.LAST_VALUE)
print(s1.jump_to_value(5))
