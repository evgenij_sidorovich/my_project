# Создать таблицу департаментов.
# Departments содержит
# - name
# - manager_id
# - location_id

import sqlite3


def departments():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
            CREATE TABLE IF NOT EXISTS Departments (
            name TEXT INTEGER PRIMARY KEY,
            manager_id  TEXT, 
            location_id TEXT
            );
        """

    curs.execute(query)
    res = curs.fetchall()
    print(res)


def insert_department():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()
    query = """
            INSERT INTO Departments (name, manager_id, location_id)
            VALUES
            ('finance', '42/fin', 'Belarus'),
            ('PR', '12/PR', 'Poland'),
            ('promotion', '8/pr', 'Ukraine'),
            ('logistic', '14/log', 'Belarus'),
            ('control', '62/con', 'Russia')            

            """

    curs.execute(query)

    query = """
            select * from Departments;
            """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('COMMIT')


def update_department():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
            UPDATE Departments
            SET  location_id = 'Moldova' where name = 'logistic'
    """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    query = """
            select * from Departments;
            """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('Commit')


def delete_department():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
            DELETE from Departments
            where location_id = 'Russia'
    """
    curs.execute(query)

    query = """
                select * from Departments;
                """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('Commit')


# departments()
# insert_department()
# update_department()
delete_department()
