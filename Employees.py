# Таблицу работников Employees
# - first_name
# - last_name
# - email
# - phone_number
# - hire_date
# - job_id
# - manager_id
# - department_id

import sqlite3


def create_employees():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
                CREATE TABLE IF NOT EXISTS Employees (
                manager_id INTEGER PRIMARY KEY UNIQUE,
                first_name TEXT,
                last_name TEXT,
                email TEXT, 
                phone_number TEXT,
                hire_date Text,
                job_id TEXT, 
                department_id TEXT
                );
            """

    curs.execute(query)
    res = curs.fetchall()
    print(res)


def insert_person():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()
    query = """
            INSERT INTO Employees (manager_id, first_name, last_name, email,
            phone_number, hire_date, job_id, department_id)
            VALUES
            (245, 'Petr', 'Smolov', 'Smolov@gmail.com', '80291112233', '12-04-2019', 'manager', '42/fin'),
            (152, 'Ben', 'Foster', 'Ben_Fos@gmail.com', '73424566431', '14-09-2000', 'master', '62/con'),
            (132, 'Alan', 'Smith', 'AlSm@rambler.ru', '34324532335', '28-11-2016', 'pr-manager', '8/pr'),
            (82, 'Alexandra', 'Smirnova', 'AlexaSm@tut.by', '80299987654', '11-16-2020', 'HR', '12/pr'),
            (123, 'Olga', 'Petrova', 'PetrovaOlga92@gmail.com', '80334345672', '03-05-2011', 'manager', '42/fin'),
            (120, 'Anna', 'Samsonova', 'AnSams23@rambler.ru', '52532335', '21-10-2019', 'pr-manager', '8/pr')

            """

    curs.execute(query)

    query = """
            select * from Employees;
            """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('Commit')


def update_date():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
            UPDATE Employees
            SET  job_id = 'worker' where job_id = 'master'
    """
    curs.execute(query)

    query = """
                select * from Employees;
                """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('Commit')


def delete_date():
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
            DELETE from Employees
            where job_id = 'worker'
    """
    curs.execute(query)

    query = """
                select * from Employees;
                """
    curs.execute(query)
    res = curs.fetchall()
    print(res)

    curs.execute('Commit')


#
# create_employees()
# insert_person()
# update_date()
# delete_date()


# Реализовать функцию, возвращающую количество работников в отделе

def count_worker():
    """Function return count workers in employers"""
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
                select count(*) from Employees as count;
                    """

    curs.execute(query)
    res = curs.fetchall()
    print(res)


count_worker()


# Реализовать функцию, возвращающую количество руководителей (manager)

def count_manager():
    """Function return count workers, who have job_id manager"""
    connect = sqlite3.connect("hw_19.sqlite")
    curs = connect.cursor()

    query = """
                select count(*) from Employees as count
                where job_id = 'manager';
                    """

    curs.execute(query)
    res = curs.fetchall()
    print(res)


count_manager()
