# -- Ex.1
# -- Найти заказчиков(customer) и обслуживающих их заказы сотрудкников(employees)
# -- Заказчики и сотрудники из города London, доставка осуществляется компанией Speedy Express.
# -- Вывести компанию заказчика и ФИО сотрудника.

import psycopg2
from session import open_db_session
from models import Customers, Employees, Orders


def get_customer_and_employeer():
    with open_db_session() as session:
        query = (session.query(Customers.company_name, Employees.last_name, Employees.first_name).distinct().
                 join(Orders, Customers.customer_id == Orders.customer_id).
                 join(Employees, Orders.employee_id == Employees.employee_id).
                 filter(Customers.city == 'London',
                        Employees.city == 'London',
                        Customers.company_name == 'Speedy Express').all())

        for i in query:
            print(i)


get_customer_and_employeer()


def get_customer_with_employer():
    conn = psycopg2.connect(dbname='tech', user='tech',
                            password='tech', host='localhost', port='8432')
    cursor = conn.cursor()

    query = """ 
            select distinct c.company_name, e.last_name, e.first_name from customers as c
            inner join orders o using (customer_id)
            inner join employees e using (employee_id)
            where c.city = 'London' and e.city = 'London' and c.company_name = 'Speedy Express'
        """

    cursor.execute(query)
    res = cursor.fetchall()
    print(res)

    cursor.execute('COMMIT')


get_customer_with_employer()
