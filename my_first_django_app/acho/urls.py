from django.urls import re_path, path
from .views import random_number, HistView, RangeView, hist_details, HistViewSet

urlpatterns = [
    re_path(r'^$', random_number, name='random_number'),
    path(r'history/', HistView.as_view(), name='HistView'),
    re_path(r'^range/(?P<object_id>\d+)/$', RangeView.as_view(), name='list_random_numbers'),
    path(r'history/<int:id>/', hist_details, name='details'),
    re_path(r'^json_api/', HistViewSet.as_view(), name='json_hist')
]
