from django.db import models


class RequestHistory(models.Model):
    data = models.DateTimeField()
    url = models.URLField()


    class Meta:
        ordering = ('-data', )

    def __str__(self):
        return f'data={self.data.isoformat()}, url={self.url}'

    def to_dict(self):
        return {
            'data': self.data.isoformat(),
            'url': self.url,
        }
