from rest_framework.serializers import ModelSerializer

from .models import RequestHistory


class HistSerializer(ModelSerializer):
    class Meta:
        model = RequestHistory
        fields = "__all__"
