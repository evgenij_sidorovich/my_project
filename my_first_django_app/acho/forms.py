from django import forms

from .models import RequestHistory


class HistForm(forms.ModelForm):
    Data = forms.DateTimeField(label="Дата")
    url = forms.URLField(label="Url адрес")

    class Meta:
        model = RequestHistory
        fields = '__all__'
