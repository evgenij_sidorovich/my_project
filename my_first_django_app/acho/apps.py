from django.apps import AppConfig


class AchoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'acho'
