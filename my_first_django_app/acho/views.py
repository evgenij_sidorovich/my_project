from django.http.request import HttpRequest
from django.http.response import HttpResponse
from random import randint

from django.shortcuts import get_object_or_404, render
from django.views import generic
from django.views.generic.base import TemplateView
from django.views.generic import ListView
import datetime
import json


from rest_framework.generics import (
    ListAPIView, CreateAPIView
)
from .models import RequestHistory
from .serializers import HistSerializer


def random_number(request):
    result = randint(1, 100)
    RequestHistory.objects.create(data=datetime.datetime.now(), url=request.path)
    return HttpResponse(result)


class HistView(generic.ListView):
    template_name = 'hist.html'
    paginate_by = 20
    model = RequestHistory
    context_object_name = 'history'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_hist'] = RequestHistory.objects.all()
        return context

    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)


class RangeView(TemplateView):
    template_name = 'rangeview.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list_data'] = range(int(kwargs.get('object_id', 5)))
        return context

    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)


def hist_details(request, id):
    my_object = get_object_or_404(RequestHistory, id=id)
    return render(request, 'details.html', {'my_object': my_object})


class HistViewSet(ListAPIView):
    queryset = RequestHistory.objects.all()
    serializer_class = HistSerializer

#
# class CreateHistView(CreateAPIView):
#     queryset = Good.objects.all()
#     serializer_class = GoodSerializer