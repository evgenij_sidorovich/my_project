# Ex.3
# Реализовать декоратор кэширования вызова функции
# В случае, если вызывается функция c одинаковыми
# параметрами, то результат не должен заново вычисляться,
# а возвращаться из хранилища

from functools import lru_cache


@lru_cache
def summ_two_numbers(number_1: int, number_2: int):
    return number_1 + number_2


summ_two_numbers(2, 3)
summ_two_numbers(4, 5)
summ_two_numbers(4, 5)
summ_two_numbers(4, 5)
summ_two_numbers(1, 5)

info_cache = summ_two_numbers.cache_info()
print(info_cache)


def test_summ():
    assert summ_two_numbers(1, 1) == 2
