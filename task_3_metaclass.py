# Ex.3
# Реализовать мета-класс фабрику, которая
# в зависимости от атрибута shape_type = ['Rectange', 'Triangle', 'Circle']
# возвращает нужный класс с свойством square (площадь фигуры)

import math


def Rectangle_square(self, side_1, side_2):
    return side_1 * side_2


def Triangle_square(self, base, hight):
    return (base * hight) / 2


def Circle_square(self, radius):
    return math.pi * radius**2


shape_type = {
    '1': (Rectangle_square, 'Rectangle'),
    '2': (Triangle_square, 'Triangle'),
    '3': (Circle_square, 'Circle'),
}


def create_fabric(args):
    if args in shape_type:
        meth, class_name = shape_type[args]

        return type(
            class_name,
            (),
            {meth.__name__: meth}
        )


CustomCls = create_fabric('1')
inst = CustomCls()
print(inst.Rectangle_square(3, 4))

CustomCls = create_fabric('2')
inst = CustomCls()
print(inst.Triangle_square(5, 3))

CustomCls = create_fabric('3')
inst = CustomCls()
print(inst.Circle_square(2))
