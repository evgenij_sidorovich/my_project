import psycopg2


def get_customer_and_employeer():

    conn = psycopg2.connect(dbname='tech', user='tech',
                            password='tech', host='localhost', port='8432')
    cursor = conn.cursor()

    query = """ 
            select distinct c.company_name, e.last_name, e.first_name from customers as c
            inner join orders o using (customer_id)
            inner join employees e using (employee_id)
            where c.city = 'London' and e.city = 'London' and c.company_name = 'Speedy Express'
        """
        
    cursor.execute(query)
    res = cursor.fetchall()
    print(res)

    cursor.execute('COMMIT')


get_customer_and_employeer()
