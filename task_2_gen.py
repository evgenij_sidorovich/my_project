# Ex2
# Реализовать вычисление факториала через генератор

def gen_numbers():
    counter = 1
    while True:
        yield counter
        counter += 1


gen = gen_numbers()

factorial_number = 1
for _ in range(5):
    factorial_number *= next(gen)


print(factorial_number)
