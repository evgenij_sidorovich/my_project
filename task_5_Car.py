# Ex.5
# Реализовать класс Car в соответствии с интерфейсом.
# В нем реализовать метод speed, возвращающий текущую скорость.
# Реализовать декоратор метода - в случае превышения скорости - декоратор
# должен логировать в logger.error сообщение о превышении лимита

from task_6_spoiler import Spoiler


class Car(object):
    def __init__(self, brand, task_6_spoiler: Spoiler = None):
        self.brand = brand
        self.current_speed = 0
        self._max_speed = 240
        self.spoiler = task_6_spoiler

    def decor_speed(func):
        def wrapper(self, *args, **kwargs):
            res = func(self, *args, **kwargs)
            if self.current_speed <= self._max_speed:
                print(f"Now moving with speed {self.current_speed} kmh")
            else:
                print("Car is moving with the highest speed possible")
            return res

        return wrapper

    def forward(self):
        print(f'Car {self.brand} start of moving')

    def stop(self):
        print(f'Car {self.brand} stopped')

    def speed(self):
        return self.current_speed

    @decor_speed
    def increase_speed(self, value):
        self.current_speed += value
        return self.current_speed


car_1 = Car('BMW')
car_1.forward()
car_1.increase_speed(170)
car_1.speed()

car_2 = Car('Ferrari')
car_2.forward()
car_2.increase_speed(260)
car_2.speed()
